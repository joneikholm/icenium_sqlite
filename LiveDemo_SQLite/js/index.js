

document.addEventListener("deviceready", onDeviceReady, false);
var db="teststring";
 var resultDIV= document.getElementById("simpleresult");

function onDeviceReady(){
        openDB();  // will only create, if DB is not created already
   
     createTable();  //will only create, if it doesn't exist already
    refresh();
}


function openDB(){
   
    try{
    db=window.sqlitePlugin.openDatabase({name: "testdb"});
     } catch(e){
         alert("error "+e);
     }
        
    
    
}



function createTable(){
  
    db.transaction( function(tx){
 tx.executeSql("CREATE TABLE IF NOT EXISTS todo (ID INTEGER PRIMARY KEY ASC, todo TEXT, added_on DATETIME)"
        ,[], onSuccess, fail);
    }
    
    );
    
}

function addTodo(todoText){
    db.transaction(function(tx){
        var added_on= new Date();
        tx.executeSql("INSERT INTO todo (todo, added_on) VALUES (?,?)", [todoText,added_on ], onSuccessTodo, fail);
       
    }
    );
    
}

function onSuccess(transaction, result){
   
}
function onSuccessTodo(transaction, result){
       
    refresh();
}


function refresh(){
     
    var renderTodo=function(row){
       
        
      return "<p>"+row.todo+" date:"+row.added_on+"</p>";
       
    }
    
    var render=function(tx, rs){
            var rowOutput="";  
        for(var i=0; i<rs.rows.length; i++){
            rowOutput += renderTodo(rs.rows.item(i));
        }
           resultDIV.innerHTML = "text: "+rowOutput;
       

    }
   
    db.transaction(function(tx){ 
        tx.executeSql("SELECT * FROM todo", [], render, fail);
       
    });
    //resultDIV.innerHTML="here comes the real result later";
}

function fail(error){
 alert(error.code + " message: "+error.message);   
}

function createTodo(){
    var todo = document.getElementById("todo");    
	addTodo(todo.value);
	todo.value = "";
    
}















































